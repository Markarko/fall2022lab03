// 2133905 Marko Litovchenko

package linearalgebra;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
public class Vector3dTests {

    @Test
    public void testGetX(){
        Vector3d vec = new Vector3d(3, 2, 2);
        assertEquals(3, vec.getX(), 0.1);
    }

    @Test
    public void testGetY(){
        Vector3d vec = new Vector3d(2, 3, 2);
        assertEquals(3, vec.getY(), 0.1);
    }

    @Test
    public void testGetZ(){
        Vector3d vec = new Vector3d(2, 2, 3);
        assertEquals(3, vec.getZ(), 0.1);
    }

    @Test
    public void testMagnitude(){
        Vector3d vec = new Vector3d(2, 2, 2);
        assertEquals(Math.sqrt(12), vec.magnitude(), 0.1);
    }

    @Test
    public void testDotProduct(){
        Vector3d vec = new Vector3d(2, 2, 2);
        Vector3d vec2 = new Vector3d(3, 3, 3);
        assertEquals(18, vec.dotProduct(vec2), 0.1);
    }

    @Test
    public void testAdd(){
        Vector3d vec = new Vector3d(2, 2, 2);
        Vector3d vec2 = new Vector3d(3, 3, 3);
        Vector3d expected = vec.add(vec2);
        Vector3d output = new Vector3d(5, 5, 5);
        double expectedDouble = expected.getX() * 100 + expected.getY() * 10 + expected.getZ();
        double outputDouble = output.getX() * 100 + output.getY() * 10 + output.getZ();
        assertEquals(expectedDouble, outputDouble, 0.1);
    }
}
