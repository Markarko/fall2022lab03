// 2133905 Marko Litovchenko

package linearalgebra;

public class Vector3d {
    private final double x;
    private final double y;
    private final double z;

    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public double getX(){
        return this.x;
    }
    public double getY(){
        return this.y;
    }
    public double getZ(){
        return this.z;
    }
    public double magnitude(){
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2));
    }
    public double dotProduct(Vector3d vec){
        return this.x * vec.getX() + this.y * vec.getY() + this.z * vec.getZ();
    }
    public Vector3d add(Vector3d vec){
        return new Vector3d(this.x + vec.getX(), this.y + vec.getY(), this.z + vec.getZ());
    }
}
